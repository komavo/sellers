//@dart=2.9

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:new_version/new_version.dart';
import 'utils/variables.dart';
import 'utils/buttons.dart';
import 'screens/login/login.dart';
import 'screens/apply/applyAccount.dart';

class LandingScreen extends StatefulWidget {
  const LandingScreen({Key key}) : super(key: key);

  @override
  State<LandingScreen> createState() => _LandingScreenState();
}

class _LandingScreenState extends State<LandingScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _checkVersion();
  }

  void _checkVersion() async {
    final newVersion = NewVersion(
      androidId: "com.komavo.komavo_sellers",
    );
    final status = await newVersion.getVersionStatus();
    if (status.localVersion != status.storeVersion) {
      newVersion.showUpdateDialog(
        context: context,
        versionStatus: status,
        dialogTitle: "UPDATE!!!",
        dismissButtonText: "Skip",
        dialogText: "Please update the app from " +
            "${status.localVersion}" +
            " to " +
            "${status.storeVersion}",
        dismissAction: () {
          SystemNavigator.pop();
        },
        updateButtonText: "Lets update",
      );
    }

    print("DEVICE : " + status.localVersion);
    print("STORE : " + status.storeVersion);
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
        backgroundColor: primaryColor,
        body: SingleChildScrollView(
          child: Container(
            alignment: Alignment.center,
            child: Column(
              children: [
                SizedBox(height: 50.h),
                Container(
                  padding: EdgeInsets.all(20),
                  child: Image.asset('assets/images/landing-hero.png'),
                ),
                Container(
                    padding: EdgeInsets.all(30),
                    color: primaryColor,
                    child: Column(
                      children: [
                        Image.asset('assets/images/komavo_secondary.png',
                            width: 250.w),
                        SizedBox(height: 50.h),
                        CustomButton(
                            buttonText: 'I have an account',
                            type: 'secondary',
                            icon: Icon(Feather.chevron_right),
                            callback: () => {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => LoginScreen()))
                                }),
                        SizedBox(height: 20.h),
                        CustomButton(
                            buttonText: 'I want to apply',
                            type: 'secondary',
                            icon: Icon(Feather.chevron_right),
                            callback: () => {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => ApplyAccount()))
                                }),
                      ],
                    )),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
