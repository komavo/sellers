import 'screen2.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_icons/flutter_icons.dart';

import '../utils/variables.dart';
import '../pages/home_page.dart';
import '../pages/profile_page.dart';
import '../pages/menu/menu_page.dart';
import '../pages/home/home.dart';
import '../pages/orders/orders.dart';
import '../pages/menu/add_menu_item.dart';

class MainScreen extends StatefulWidget {
  int selectedIndex;
  MainScreen({Key? key, required this.selectedIndex}) : super(key: key);
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  // int selectedIndex = 0;

  List<GlobalKey<NavigatorState>> _navigatorKeys = [
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>()
  ];

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        final isFirstRouteInCurrentTab =
            await _navigatorKeys[widget.selectedIndex].currentState!.maybePop();

        print(
            'isFirstRouteInCurrentTab: ' + isFirstRouteInCurrentTab.toString());

        // let system handle back button if we're on the first route
        return isFirstRouteInCurrentTab;
      },
      child: Scaffold(
        backgroundColor: Color(0xff222222),
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Color(0xff222222),
          currentIndex: widget.selectedIndex,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Feather.home, color: Colors.white54),
              label: "Home",
              activeIcon: Icon(
                Feather.home,
                color: primaryColor,
                size: 30,
              ),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                FontAwesome.list_alt,
                color: Colors.white54,
              ),
              label: "Menu",
              activeIcon: Icon(
                FontAwesome.list_alt,
                color: primaryColor,
                size: 30,
              ),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Feather.activity,
                color: Colors.white54,
              ),
              label: "Orders",
              activeIcon: Icon(
                Feather.activity,
                color: primaryColor,
                size: 30,
              ),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Feather.user,
                color: Colors.white54,
              ),
              label: "User",
              activeIcon: Icon(
                Feather.user,
                color: primaryColor,
                size: 30,
              ),
            ),
          ],
          onTap: (index) {
            setState(() {
              widget.selectedIndex = index;
            });
          },
        ),
        body: Stack(
          children: [
            _buildOffstageNavigator(0),
            _buildOffstageNavigator(1),
            _buildOffstageNavigator(2),
            _buildOffstageNavigator(3),
          ],
        ),
      ),
    );
  }

  void _pushAddMenuItem() {
    Navigator.push(
        context, CupertinoPageRoute(builder: (context) => AddMenuItem()));
  }

  Map<String, WidgetBuilder> _routeBuilders(BuildContext context, int index) {
    return {
      '/': (context) {
        return [
          HomeScreen(),
          MenuPage(
            onAddMenuItem: _pushAddMenuItem,
          ),
          OrderListPage(),
          ProfilePage(),
        ].elementAt(index);
      },
    };
  }

  Widget _buildOffstageNavigator(int index) {
    var routeBuilders = _routeBuilders(context, index);

    return Offstage(
      offstage: widget.selectedIndex != index,
      child: Navigator(
        key: _navigatorKeys[index],
        onGenerateRoute: (routeSettings) {
          return CupertinoPageRoute(
            builder: (context) => routeBuilders[routeSettings.name]!(context),
          );
        },
      ),
    );
  }
}
