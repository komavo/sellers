import '../../utils/variables.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import '../../utils/shared_preferences.dart';

apiSendOTP(phone) async {
  final response = await http.post(
    Uri.parse(baseURL + '/auth/login'),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{'user_type': 'business', 'phone': phone}),
  );

  if (response.statusCode == 200) {
    final temp = jsonDecode(response.body);
    print(temp);
    if (temp['error'] == true) {
      if (temp['message'] == 'User not found') {
        return false;
      }
    }
    return true;
    // return Album.fromJson(jsonDecode(response.body));
  } else {
    throw Exception('There was an error loggin in');
  }
}

verifyOTP(otp, phone) async {
  final response = await http.post(
    Uri.parse(baseURL + '/auth/login'),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(
        <String, String>{'user_type': 'business', 'phone': phone, 'otp': otp}),
  );

  if (response.statusCode == 200) {
    final temp = jsonDecode(response.body);
    print(temp);
    if (temp['status_code'] == 200) {
      MySharedPreferences.instance
          .setStringValue('id', temp['data']['id'].toString());
      MySharedPreferences.instance
          .setStringValue('access_token', temp['data']['access_token']);
      MySharedPreferences.instance
          .setStringValue('first_name', temp['data']['first_name']);
      MySharedPreferences.instance
          .setStringValue('last_name', temp['data']['last_name']);
      MySharedPreferences.instance
          .setStringValue('phone', temp['data']['phone'].toString());
      MySharedPreferences.instance.setBooleanValue('loggedIn', true);

      MySharedPreferences.instance
          .setBooleanValue('is_open', temp['data']['is_open']);
    }
    return true;
    // return Album.fromJson(jsonDecode(response.body));
  } else {
    throw Exception('Failed to load album');
  }
}

apiVerifyOTP(otp, phone) async {
  return await verifyOTP(otp, phone);
}
