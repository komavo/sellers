import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:komavo_seller/utils/buttons.dart';
import 'package:otp_text_field/otp_text_field.dart';
import './api.dart';
import '../../utils/variables.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';
import '../main_screen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool _otpSent = false;
  String _phone = '';
  bool userNotFoundError = false;
  _update(phone) async {
    _phone = phone;
    var response = await apiSendOTP(phone);
    print(response);
    if (!response) {
      showDialog(
          barrierDismissible:
              true, //tapping outside dialog will close the dialog if set 'true'
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Text('Oops!'),
              content: const Text(
                  'We couldn\'t find any account with that phone number, please check the number and try again'),
              actions: <Widget>[
                CustomButton(
                    buttonText: 'Okay',
                    type: 'primary',
                    callback: () => {Navigator.of(context).pop()},
                    icon: Icon(Feather.check)),
              ],
            );
          });
    } else {
      setState(() {
        _otpSent = true;
      });
    }
  }

  void _verifyOTP(otp) async {
    await apiVerifyOTP(otp, _phone);
    Navigator.pushReplacement(
        context,
        CupertinoPageRoute(
            builder: (context) => MainScreen(
                  selectedIndex: 0,
                )));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: primaryColor,
      body: SingleChildScrollView(
        child: Container(
          alignment: Alignment.center,
          child: Column(
            children: [
              SizedBox(height: 100.h),
              Container(
                  child: Image.asset('assets/images/komavo_secondary.png',
                      width: 300.w)),
              SizedBox(height: 30.h),
              Container(
                child: (_otpSent
                    ? VerifyOtp(verifyOTP: _verifyOTP)
                    : CollectOtp(
                        update: _update,
                      )),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class CollectOtp extends StatelessWidget {
  // const CollectOtp({Key? key}) : super(key: key);

  final phoneNumberController = TextEditingController();

  final ValueChanged update;
  CollectOtp({required this.update});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(children: [
        Container(
            padding: EdgeInsets.all(50),
            child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.white,
                ),
                child: Center(
                    child: TextFormField(
                  controller: phoneNumberController,
                  style: TextStyle(
                      fontSize: 30.sp,
                      color: Color(0xff666666),
                      letterSpacing: 3,
                      fontFamily: 'Arial'),
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[
                    new LengthLimitingTextInputFormatter(10),
                    FilteringTextInputFormatter.allow(RegExp(r'[0-9]|10'))
                  ],
                  decoration: InputDecoration(
                    prefixIcon: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Image.asset(
                        'assets/images/india_flag.png',
                        width: 20.w,
                        height: 20.h,
                        fit: BoxFit.fill,
                      ),
                    ),
                    prefixText: '+91   ',
                    prefixStyle: TextStyle(
                        color: Color(0xff999999),
                        fontSize: 20.sp,
                        fontFamily: 'Arial'),
                    // icon: Image.asset(name),
                    hintText: 'Phone Number',
                    hintStyle:
                        TextStyle(fontSize: 20.sp, color: Color(0xffaaaaaa)),
                    contentPadding: EdgeInsets.all(10),
                    border: InputBorder.none,
                  ),
                  onSaved: (String? value) {
                    // This optional block of code can be used to run
                    // code when the user saves the form.
                  },
                )))),
        Directionality(
          textDirection: TextDirection.rtl,
          child: ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                  padding:
                      EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.h),
                  primary: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20))),
              onPressed: () => update(phoneNumberController.text),
              label: Text('Send OTP',
                  style: TextStyle(color: Color(0xff666666), fontSize: 20.sp)),
              icon: Icon(
                Icons.navigate_before_rounded,
                color: primaryColor,
              )),
        )
      ]),
    );
  }
}

class VerifyOtp extends StatelessWidget {
  // const VerifyOtp({Key? key}) : super(key: key);

  final ValueChanged verifyOTP;
  VerifyOtp({required this.verifyOTP});

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      SizedBox(height: 30.h),
      Container(
          width: double.infinity,
          child: Center(
            child: OTPTextField(
              length: 6,
              width: MediaQuery.of(context).size.width,
              fieldWidth: 50,
              style: TextStyle(
                  fontSize: 20.sp,
                  color: Color(0xff666666),
                  fontWeight: FontWeight.bold),
              textFieldAlignment: MainAxisAlignment.spaceEvenly,
              fieldStyle: FieldStyle.box,
              otpFieldStyle: OtpFieldStyle(
                  backgroundColor: Colors.white,
                  borderColor: primaryColor,
                  focusBorderColor: primaryColor),
              onCompleted: (otp) => verifyOTP(otp),
            ),
          )),
      SizedBox(height: 30.h),
      Center(
        child: Text('Please enter the OTP',
            style: TextStyle(
                color: Colors.white,
                fontSize: 20.sp,
                fontWeight: FontWeight.bold)),
      )
    ]);
  }
}
