import '../../utils/variables.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import '../../utils/shared_preferences.dart';

postAccountApplication(phone, firstName, lastName, storeName, email, address,
    storeImage, storeImageMIME) async {
  final response = await http.post(
    Uri.parse(baseURL + '/business/apply'),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
      'first_name': firstName,
      'last_name': lastName,
      'phone': phone,
      'store_name': storeName,
      'email': email,
      'address': address,
      'store_image': storeImage,
      'store_image_MIME': storeImageMIME
    }),
  );
  if (response.statusCode == 200) {
    final temp = jsonDecode(response.body);
    print(temp);
    MySharedPreferences.instance.setStringValue('id', temp['data']['id']);
    MySharedPreferences.instance
        .setStringValue('first_name', temp['data']['first_name']);
    MySharedPreferences.instance
        .setStringValue('last_name', temp['data']['last_name']);
    MySharedPreferences.instance
        .setStringValue('phone', temp['data']['phone'].toString());
    MySharedPreferences.instance
        .setStringValue('store_image', temp['data']['store_image']);
    return;
  } else {
    throw Exception('There was an error processing');
  }
}
