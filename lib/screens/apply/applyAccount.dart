import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:komavo_seller/screens/apply/holdingScreen.dart';
import 'package:komavo_seller/utils/buttons.dart';
import 'package:komavo_seller/utils/variables.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:mime/mime.dart';
import 'dart:io';
import 'dart:convert';
import './api.dart';

class ApplyAccount extends StatefulWidget {
  const ApplyAccount({Key? key}) : super(key: key);

  @override
  _ApplyAccountState createState() => _ApplyAccountState();
}

class _ApplyAccountState extends State<ApplyAccount> {
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final phoneController = TextEditingController();
  final storeNameController = TextEditingController();
  final emailController = TextEditingController();
  final addressController = TextEditingController();

  String imagePath = "";
  bool noImageError = false;
  final picker = ImagePicker();

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: primaryColor,
        body: SingleChildScrollView(
          child: Container(
            child: Column(children: [
              SizedBox(height: 70),
              Image.asset('assets/images/komavo_secondary.png', width: 200),
              Container(
                  margin: EdgeInsets.only(top: 30),
                  padding: EdgeInsets.all(20),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30)),
                  ),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.all(10),
                          child: Text(
                            "We'll need some information about you. Don't worry this will be quick, promise ;)",
                            style: TextStyle(
                                fontSize: 20, color: Color(0xff666666)),
                          ),
                        ),
                        Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.white,
                            ),
                            margin: EdgeInsets.only(top: 20),
                            child: Center(
                                child: TextFormField(
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'We really need this information';
                                }
                                return null;
                              },
                              textAlign: TextAlign.left,
                              controller: phoneController,
                              style: TextStyle(
                                  fontSize: 20, color: Color(0xff666666)),
                              keyboardType: TextInputType.number,
                              inputFormatters: <TextInputFormatter>[
                                new LengthLimitingTextInputFormatter(10),
                                FilteringTextInputFormatter.allow(
                                    RegExp(r'[0-9]|10'))
                              ],
                              decoration: InputDecoration(
                                labelText: 'Phone Number',
                                labelStyle: TextStyle(color: primaryColor),
                                hintText: 'We won\'t spam, promise!',
                                hintStyle: TextStyle(
                                    fontSize: 20, color: Color(0xffaaaaaa)),
                                contentPadding: EdgeInsets.all(10),
                                border: InputBorder.none,
                              ),
                              onSaved: (String? value) {
                                // This optional block of code can be used to run
                                // code when the user saves the form.
                              },
                            ))),
                        Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.white,
                            ),
                            child: Center(
                                child: TextFormField(
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'This is required';
                                }
                                return null;
                              },
                              textAlign: TextAlign.left,
                              controller: firstNameController,
                              style: TextStyle(
                                  fontSize: 20, color: Color(0xff666666)),
                              decoration: InputDecoration(
                                labelText: 'First Name',
                                labelStyle: TextStyle(color: primaryColor),
                                hintText: 'Tell us your first name',
                                hintStyle: TextStyle(
                                    fontSize: 20, color: Color(0xffaaaaaa)),
                                contentPadding: EdgeInsets.all(10),
                                border: InputBorder.none,
                              ),
                              onSaved: (String? value) {
                                // This optional block of code can be used to run
                                // code when the user saves the form.
                              },
                            ))),
                        Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.white,
                            ),
                            child: Center(
                                child: TextFormField(
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'This is required';
                                }
                                return null;
                              },
                              textAlign: TextAlign.left,
                              controller: lastNameController,
                              style: TextStyle(
                                  fontSize: 20, color: Color(0xff666666)),
                              decoration: InputDecoration(
                                labelText: 'Last Name',
                                labelStyle: TextStyle(color: primaryColor),
                                hintText: 'Your last name',
                                hintStyle: TextStyle(
                                    fontSize: 20, color: Color(0xffaaaaaa)),
                                contentPadding: EdgeInsets.all(10),
                                border: InputBorder.none,
                              ),
                              onSaved: (String? value) {
                                // This optional block of code can be used to run
                                // code when the user saves the form.
                              },
                            ))),
                        Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.white,
                            ),
                            child: Center(
                                child: TextFormField(
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'This is required';
                                }
                                return null;
                              },
                              textAlign: TextAlign.left,
                              controller: emailController,
                              style: TextStyle(
                                  fontSize: 20, color: Color(0xff666666)),
                              decoration: InputDecoration(
                                labelText: 'Email',
                                labelStyle: TextStyle(color: primaryColor),
                                hintText: 'Your email',
                                hintStyle: TextStyle(
                                    fontSize: 20, color: Color(0xffaaaaaa)),
                                contentPadding: EdgeInsets.all(10),
                                border: InputBorder.none,
                              ),
                              onSaved: (String? value) {
                                // This optional block of code can be used to run
                                // code when the user saves the form.
                              },
                            ))),
                        Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.white,
                            ),
                            child: Center(
                                child: TextFormField(
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'This is required';
                                }
                                return null;
                              },
                              textAlign: TextAlign.left,
                              controller: storeNameController,
                              style: TextStyle(
                                  fontSize: 20, color: Color(0xff666666)),
                              decoration: InputDecoration(
                                labelText: 'Restaurant name',
                                labelStyle: TextStyle(color: primaryColor),
                                hintText: 'Your restaurant\'s name?',
                                hintStyle: TextStyle(
                                    fontSize: 20, color: Color(0xffaaaaaa)),
                                contentPadding: EdgeInsets.all(10),
                                border: InputBorder.none,
                              ),
                              onSaved: (String? value) {
                                // This optional block of code can be used to run
                                // code when the user saves the form.
                              },
                            ))),
                        Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.white,
                            ),
                            child: Center(
                                child: TextFormField(
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'This is required';
                                }
                                return null;
                              },
                              keyboardType: TextInputType.multiline,
                              maxLines: null,
                              textAlign: TextAlign.left,
                              controller: addressController,
                              style: TextStyle(
                                  fontSize: 20, color: Color(0xff666666)),
                              decoration: InputDecoration(
                                labelText: 'Address',
                                labelStyle: TextStyle(color: primaryColor),
                                hintText: 'Where do we find your restaurant?',
                                hintStyle: TextStyle(
                                    fontSize: 20, color: Color(0xffaaaaaa)),
                                contentPadding: EdgeInsets.all(10),
                                border: InputBorder.none,
                              ),
                              onSaved: (String? value) {
                                // This optional block of code can be used to run
                                // code when the user saves the form.
                              },
                            ))),
                        SizedBox(height: 20),
                        imagePath == ""
                            ? Image.asset('assets/images/store-placeholder.png')
                            : Image.file(File(imagePath)),
                        SizedBox(height: 20),
                        noImageError
                            ? Text(
                                "An image representing your restaurant is required",
                                style: TextStyle(color: Colors.redAccent),
                              )
                            : Container(),
                        Center(
                            child: CustomButton(
                          buttonText: 'Upload an image',
                          type: 'secondary',
                          icon: Icon(Feather.upload_cloud),
                          callback: () async {
                            final pickedFile = await picker.pickImage(
                                source: ImageSource.gallery);
                            if (pickedFile != null) {
                              File? croppedFile = await ImageCropper.cropImage(
                                sourcePath: pickedFile.path,
                                aspectRatioPresets: [
                                  // CropAspectRatioPreset.square,
                                  // CropAspectRatioPreset.ratio3x2,
                                  // CropAspectRatioPreset.original,
                                  // CropAspectRatioPreset.ratio4x3,
                                  CropAspectRatioPreset.ratio16x9
                                ],
                                androidUiSettings: AndroidUiSettings(
                                  toolbarTitle: 'Crop Image',
                                  toolbarColor: primaryColor,
                                  toolbarWidgetColor: Colors.white,
                                  activeControlsWidgetColor: primaryColor,
                                  initAspectRatio:
                                      CropAspectRatioPreset.original,
                                  lockAspectRatio: false,
                                ),
                                iosUiSettings: IOSUiSettings(
                                  minimumAspectRatio: 1.0,
                                ),
                              );
                              if (croppedFile != null) {
                                setState(() {
                                  imagePath = croppedFile.path;
                                });
                              }
                            }
                          },
                        )),
                        SizedBox(
                          height: 50,
                        ),
                        CustomButton(
                          buttonText: 'Submit my application',
                          type: 'primary',
                          callback: () {
                            if (_formKey.currentState!.validate()) {
                              if (imagePath == "") {
                                setState(() {
                                  noImageError = true;
                                });
                                return;
                              } else {
                                postAccountApplication(
                                    phoneController.text,
                                    firstNameController.text,
                                    firstNameController.text,
                                    storeNameController.text,
                                    emailController.text,
                                    addressController.text,
                                    base64Encode(
                                        File(imagePath).readAsBytesSync()),
                                    lookupMimeType(imagePath));
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => HoldingScreen()));
                              }
                            }
                          },
                          icon: Icon(Feather.chevron_right),
                        )
                      ],
                    ),
                  )),
            ]),
          ),
        ));
  }
}
