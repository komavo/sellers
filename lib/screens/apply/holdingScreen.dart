import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../utils/variables.dart';
import '../../utils/shared_preferences.dart';

class HoldingScreen extends StatefulWidget {
  const HoldingScreen({Key? key}) : super(key: key);

  @override
  _HoldingScreenState createState() => _HoldingScreenState();
}

class _HoldingScreenState extends State<HoldingScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: primaryColor,
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              SizedBox(height: 70.h),
              Center(
                child: Image.asset(
                  'assets/images/komavo_secondary.png',
                  width: 200.w,
                ),
              ),
              SizedBox(height: 30.h),
              Image.asset(
                'assets/images/undraw_verifying.png',
                width: MediaQuery.of(context).size.width - 100.w,
              ),
              SizedBox(height: 30.h),
              Center(
                  child: Text(
                "Verfication Ongoing",
                style: TextStyle(fontSize: 30.sp, color: Colors.white),
              )),
              Container(
                padding: EdgeInsets.only(right: 50.w, left: 50.w, top: 10.h),
                child: Center(
                    child: Text(
                  "Please wait, our representatives will call you shortly. This process is usually completed within 48Hrs of your application submission.",
                  style: TextStyle(fontSize: 20.sp, color: Colors.white),
                  textAlign: TextAlign.center,
                )),
              )
            ],
          ),
        ),
      ),
    );
  }
}
