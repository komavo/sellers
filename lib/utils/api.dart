import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import 'variables.dart';
import 'shared_preferences.dart';

class ApiController {
  static String? token;
  static Map<String, String>? headers;

  ApiController() {
    MySharedPreferences.instance
        .getStringValue('access_token')
        .then((value) => token = value);
    headers = <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      // 'Authorization': 'Bearer $token'
    };
  }

  post(endpoint, body) async {
    var response = await http.post(Uri.parse(baseURL + endpoint),
        headers: headers, body: jsonEncode(body));
    return jsonDecode(response.body);
    // if (response.statusCode == 200) {
    //   print(res);
    //   return res;
    // }
  }

  get(endpoint) async {
    var response =
        await http.get(Uri.parse(baseURL + endpoint), headers: headers);
    return jsonDecode(response.body);
    // if (response.statusCode == 200) {
    //   final res = jsonDecode(response.body);
    //   print(res);
    //   return res;
    // }
  }

  delete(endpoint) async {
    var response =
        await http.delete(Uri.parse(baseURL + endpoint), headers: headers);
    return jsonDecode(response.body);
  }
}
