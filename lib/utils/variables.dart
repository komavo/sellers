import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

final primaryColor = Color(0xffCD4B4B);
final darkBGColor = Colors.black;
final darkColor = Color(0xff222222);
final darkText = Color(0xff222222);
final lightText = Colors.white60;
final successColor = Color(0xff308626);

final s3_prefix = 'https://komavo-dev.s3.ap-south-1.amazonaws.com/';
final baseURL = 'https://api.komavo.in/v2';
// final baseURL = 'http://192.168.1.27:8000/v1'; //home wifi
// final baseURL = 'http://192.168.98.28:8000/v1'; //phone wifi