import 'package:flutter/material.dart';
import 'variables.dart';
import 'package:flutter_icons/flutter_icons.dart';

class BottomNav extends StatefulWidget {
  const BottomNav({Key? key}) : super(key: key);

  @override
  _BottomNavState createState() => _BottomNavState();
}

class _BottomNavState extends State<BottomNav> {
  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Feather.home),
          label: 'Home',
        ),
        BottomNavigationBarItem(
          icon: Icon(Feather.grid),
          label: 'Menu',
        ),
        BottomNavigationBarItem(
          icon: Icon(Feather.activity),
          label: 'Orders',
        ),
        BottomNavigationBarItem(
          icon: Icon(Feather.credit_card),
          label: 'Settlements',
        ),
        BottomNavigationBarItem(
          icon: Icon(Feather.user),
          label: 'Profile',
        ),
      ],
      backgroundColor: primaryColor,
      currentIndex: _selectedIndex,
      selectedItemColor: Colors.white,
      unselectedItemColor: Colors.white,
      type: BottomNavigationBarType.fixed,
      showSelectedLabels: false,
      showUnselectedLabels: false,
      onTap: _onItemTapped,
    );
  }
}
