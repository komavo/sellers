import 'package:flutter/material.dart';
import './variables.dart';

class CustomButton extends StatelessWidget {
  final String buttonText;
  final String type;
  // final Function onPressedFunc;
  final VoidCallback callback;
  final Icon icon;
  
  CustomButton(
      {required this.buttonText,
      required this.type,
      required this.callback,
      required this.icon});
  
  //styles
  final ButtonStyle _primaryStyle = ElevatedButton.styleFrom(
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 7),
      primary: primaryColor,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)));
  final ButtonStyle _darkStyle = ElevatedButton.styleFrom(
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 7),
      primary: darkColor,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)));
  final ButtonStyle _successStyle = ElevatedButton.styleFrom(
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 7),
      primary: Color(0xff308626),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)));

  getStyle(){
    switch (type) {
      case 'primary':
        return _primaryStyle;
      case 'secondary':
        return _darkStyle;
      case 'success':
        return _successStyle;
      default:
    }
  }
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Directionality(
      textDirection: TextDirection.rtl,
      child: ElevatedButton.icon(
        style: getStyle(),
        onPressed: callback,
        label: Text(buttonText,
            style: TextStyle(color: Colors.white, fontSize: 16)),
        icon: icon,
      ),
    ));
  }
}
