import 'dart:convert';
import 'package:provider/provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'shared_preferences.dart';
import 'variables.dart';

class MenuProvider with ChangeNotifier {
  void refreshItems() {
    notifyListeners();
  }
}

class OrderProvider with ChangeNotifier {
  List<dynamic> _orders = []; 

  OrderProvider(){
    getInitial();
  }

  getInitial() async {
    orders;
  }

  Future<List<dynamic>> getOrders() async {
    var userId = await MySharedPreferences.instance.getStringValue('id');
    var response =
        await http.get(Uri.parse(baseURL + '/business/order/list/$userId'));
    // print(json.decode(response.body));
    // return json.decode(response.body['data']);
    // _orders = (jsonDecode(response.body)["data"] as List).map((e) => e as Map<String, dynamic>).toList();
    return (jsonDecode(response.body)["data"] as List); //.map((e) => e as Map<String, dynamic>).toList();
  }

  get orders => [..._orders];

  void refreshOrders() async {
    await getOrders();
    notifyListeners();
  }
}
