import '../../utils/variables.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import '../../utils/shared_preferences.dart';

Future<SelfDetails> fetchStoreDetails() async {
  var userId = await MySharedPreferences.instance.getStringValue('id');
  final response = await http.get(
      Uri.parse(baseURL + '/business/detail/$userId'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      });
  if (response.statusCode == 200) {
    final res = jsonDecode(response.body);
    print(res);
    await MySharedPreferences.instance
        .setBooleanValue('is_open', res['data']['is_open']);
    return SelfDetails.fromJson(res['data']);
    // return Album.fromJson(jsonDecode(response.body));
  } else {
    throw Exception('Failed to load self details');
  }
}

updateStatus(bool status) async {
  var userId = await MySharedPreferences.instance.getStringValue('id');
  var body = jsonEncode(<String, dynamic>{
    'business_id': int.parse(userId),
    'status': status,
  });
  print(body);
  final response =
      await http.post(Uri.parse(baseURL + '/business/update-status'),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: body);
  if (response.statusCode == 200) {
    return true;
  } else {
    throw Exception('Failed to update status');
  }
}

class SelfDetails {
  final int id;
  final int phone;
  final String storeName;
  final String storeImage;
  final String firstName;
  final String lastName;
  final String email;
  final String category;
  final String address;
  final String region;
  final bool isOpen;
  final bool isVerified;
  final String verificationStatus;
  final String createdAt;
  final String updatedAt;

  SelfDetails(
      {required this.phone,
      required this.storeName,
      required this.lastName,
      required this.email,
      required this.category,
      required this.isVerified,
      required this.createdAt,
      required this.updatedAt,
      required this.id,
      required this.verificationStatus,
      required this.isOpen,
      required this.storeImage,
      required this.address,
      required this.firstName,
      required this.region});

  factory SelfDetails.fromJson(Map<String, dynamic> json) {
    return SelfDetails(
      phone: json['phone'],
      storeName: json['store_name'],
      lastName: json['last_name'],
      email: json['email'],
      category: json['category'],
      isVerified: json['is_verified'],
      createdAt: json['created_at'],
      updatedAt: json['updated_at'],
      id: json['id'],
      verificationStatus: json['verification_status'],
      isOpen: json['is_open'],
      storeImage: json['store_image'],
      address: json['address'],
      firstName: json['first_name'],
      region: json['region'],
    );
  }
}
