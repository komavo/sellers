import 'dart:ui';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:komavo_seller/main.dart';
import 'package:komavo_seller/utils/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:toggle_switch/toggle_switch.dart';
import '../../utils/variables.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'api.dart';
import '../../utils/bottomNav.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  String firstName = 'There';
  bool isOpenStatus = true;
  late Future<SelfDetails> selfDetails;

  @override
  void initState() {
    super.initState();
    selfDetails = fetchStoreDetails();
    MySharedPreferences.instance
        .getStringValue('first_name')
        .then((value) => setState(() {
              firstName = value;
            }));
    MySharedPreferences.instance.getBooleanValue('is_open').then((value) => {
          print('ISOPEN - $value'),
          setState(() {
            isOpenStatus = value;
          })
        });
  }

  updateIsOpenStatus(value) {
    updateStatus(value);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(30), bottomRight: Radius.circular(30)),
        // color: Color(0xffeeeeee),
        color: darkBGColor,
        boxShadow: [
          new BoxShadow(
            color: Colors.black,
            offset: Offset(0, 2),
            blurRadius: 15.0,
          )
        ],
      ),
      height: double.infinity,
      width: double.infinity,
      margin: EdgeInsets.only(bottom: 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 20),
          Container(
            padding: EdgeInsets.all(30),
            child: Row(
              children: [
                Text('Hi, ',
                    style: TextStyle(
                        fontSize: 20, fontFamily: 'Raleway', color: lightText)),
                Text(firstName,
                    style: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.bold,
                        color: lightText)),
                Spacer(),
                Icon(
                  Feather.bell,
                  color: Colors.white60,
                )
              ],
            ),
          ),
          Expanded(
            child: Container(
              child: FutureBuilder<SelfDetails>(
                future: selfDetails,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return ListView.builder(
                        itemCount: 1,
                        itemBuilder: (context, index) {
                          return Container(
                            height: 500.h,
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(30)),
                                color: Color(0xff222222),
                                boxShadow: [
                                  new BoxShadow(
                                    color: Color(0xff111111),
                                    blurRadius: 20.0,
                                  ),
                                ]),
                            margin: EdgeInsets.only(
                              top: 5,
                              bottom: 20,
                              left: 30,
                              right: 30,
                            ),
                            padding: EdgeInsets.all(10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  width: 400.w,
                                  height: 200.h,
                                  child: ClipRRect(
                                      borderRadius: BorderRadius.circular(20),
                                      child: CachedNetworkImage(
                                        imageUrl:
                                            '$s3_prefix${snapshot.data!.storeImage}',
                                        fit: BoxFit.cover,
                                        placeholder: (context, url) {
                                          return AspectRatio(
                                            aspectRatio: 16 / 9,
                                            child: Shimmer.fromColors(
                                                baseColor: Color(0xffdddddd),
                                                highlightColor:
                                                    Color(0xffffffff),
                                                child: Container(
                                                  height: double.infinity,
                                                  width: double.infinity,
                                                  decoration: BoxDecoration(
                                                      color: Color(0xffdddddd)),
                                                )),
                                          );
                                        },
                                      )),
                                ),
                                SizedBox(
                                  height: 20.h,
                                ),
                                Container(
                                  width: double.infinity,
                                  child: Text(
                                    snapshot.data!.storeName,
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        fontSize: 30.sp,
                                        fontWeight: FontWeight.bold,
                                        color: Color(0xff666666)),
                                  ),
                                ),
                                Spacer(),
                                Container(
                                  margin: EdgeInsets.only(bottom: 10),
                                  child: FlutterSwitch(
                                    width: 300.0,
                                    height: 50.0,
                                    valueFontSize: 15.0,
                                    toggleSize: 45.0,
                                    value: isOpenStatus,
                                    borderRadius: 30.0,
                                    padding: 8.0,
                                    showOnOff: true,
                                    activeText: 'Serving',
                                    activeColor: successColor,
                                    inactiveText: 'Not Serving',
                                    inactiveColor: primaryColor,
                                    onToggle: (val) {
                                      setState(() {
                                        isOpenStatus = val;
                                      });
                                      updateIsOpenStatus(val);
                                    },
                                  ),
                                ),
                              ],
                            ),
                          );
                        });
                  } else if (snapshot.hasError) {
                    return Text('${snapshot.error}');
                  }

                  // By default, show a loading spinner.
                  return Container();
                },
              ),
            ),
          ),
          SizedBox(height: 20),
        ],
      ),
    );
  }
}
