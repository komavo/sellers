import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:komavo_seller/pages/home/api.dart';
import 'package:komavo_seller/pages/menu/edit/edit-menu.dart';
import 'package:provider/provider.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:komavo_seller/utils/buttons.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:shimmer/shimmer.dart';
import 'package:http/http.dart' as http;
import 'package:toggle_switch/toggle_switch.dart';

import '../../utils/variables.dart';
import '../../utils/providers.dart';
import '../../screens/screen2.dart';
import '../../utils/shared_preferences.dart';
import 'add_menu_item.dart';
import 'api.dart';

class MenuPage extends StatefulWidget {
  final onAddMenuItem;
  MenuPage({Key? key, this.onAddMenuItem}) : super(key: key);

  @override
  _MenuPageState createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {
  bool inStock = false;

  String _image(dynamic menuItem) {
    if (menuItem['image'] == null) {
      return '';
    }
    return menuItem['image'];
  }

  String _name(dynamic menuItem) {
    return menuItem['name'];
  }

  String _section(dynamic menuItem) {
    if (menuItem['section'] == null) {
      return '';
    }
    return menuItem['section'];
  }

  String _price(dynamic menuItem) {
    return menuItem['price'].toString();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return listMenu();
  }

  Widget listMenu() {
    return Provider<MenuProvider>(
        create: (_) => MenuProvider(),
        builder: (context, _) {
          return Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(30),
                  bottomRight: Radius.circular(30)),
              color: darkBGColor,
              boxShadow: [
                new BoxShadow(
                  color: Colors.black,
                  offset: Offset(0, 2),
                  blurRadius: 15.0,
                )
              ],
            ),
            width: double.infinity,
            margin: EdgeInsets.only(bottom: 20),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 20),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 20, horizontal: 30),
                    child: Row(
                      children: [
                        Text('Menu',
                            style: TextStyle(
                                fontSize: 30,
                                fontWeight: FontWeight.bold,
                                color: lightText)),
                        Spacer(),
                        CustomButton(
                            buttonText: 'Add item',
                            type: 'primary',
                            callback: widget.onAddMenuItem,
                            icon: Icon(Feather.plus))
                      ],
                    ),
                  ),
                  Expanded(
                    child: FutureBuilder<List<dynamic>>(
                        future: fetchMenuItem(),
                        builder:
                            (BuildContext context, AsyncSnapshot snapshot) {
                          if (snapshot.hasData) {
                            if (snapshot.data.length == 0) {
                              return Container(
                                  margin: EdgeInsets.only(top: 100),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Image.asset('assets/images/no-menu.png',
                                          width: 200),
                                      Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 80, vertical: 20),
                                        child: Center(
                                          child: Text(
                                              'No menu items... yet! Try adding items by pressing the below button',
                                              textAlign: TextAlign.center,
                                              style: TextStyle(fontSize: 25)),
                                        ),
                                      ),
                                      Center(
                                        child: CustomButton(
                                            buttonText: 'Add menu item',
                                            type: 'primary',
                                            callback: widget.onAddMenuItem,
                                            icon: Icon(Feather.plus)),
                                      )
                                    ],
                                  ));
                            }
                            return ListView.builder(
                                physics: BouncingScrollPhysics(),
                                scrollDirection: Axis.vertical,
                                padding: EdgeInsets.symmetric(
                                    horizontal: 15, vertical: 15),
                                itemCount: snapshot.data.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return Container(
                                    margin: EdgeInsets.only(bottom: 15),
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 5, vertical: 10),
                                    decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(10)),
                                      // color: Color(0xffeeeeee),
                                      color: Color(0xff222222),
                                      boxShadow: [
                                        new BoxShadow(
                                          color: Colors.black,
                                          offset: Offset(0, 2),
                                          blurRadius: 10.0,
                                        )
                                      ],
                                    ),
                                    child: Row(
                                      children: [
                                        Stack(
                                          children: [
                                            ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(8.0),
                                              child: SizedBox(
                                                width: 90,
                                                height: 90,
                                                child: _image(snapshot
                                                            .data[index]) !=
                                                        ''
                                                    ? Image.network(
                                                        '$s3_prefix${_image(snapshot.data[index])}')
                                                    : Image.asset(
                                                        'assets/images/item-placeholder.png'),
                                              ),
                                            ),
                                            InkWell(
                                              onTap: () {
                                                print('edit pressed');
                                                setState(() {
                                                  showModalBottomSheet(
                                                      context: context,
                                                      builder: (BuildContext
                                                          context) {
                                                        return EditMenu(
                                                            inStock: snapshot
                                                                    .data[index]
                                                                ['in_stock'],
                                                            itemId: snapshot
                                                                    .data[index]
                                                                ['id'],
                                                            itemName: snapshot
                                                                    .data[index]
                                                                ['name'],
                                                            itemPrice: snapshot
                                                                    .data[index]
                                                                ['price'],
                                                            itemCategory:
                                                                snapshot.data[
                                                                        index][
                                                                    'section']);
                                                      });
                                                });
                                              },
                                              child: Ink(
                                                child: Container(
                                                  alignment:
                                                      Alignment.bottomCenter,
                                                  width: 90,
                                                  height: 100,
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              8.0)),
                                                  child: Container(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                      horizontal: 7,
                                                      vertical: 4,
                                                    ),
                                                    decoration: BoxDecoration(
                                                        color: Colors.amber,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10)),
                                                    child: Text("Edit"),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        Container(
                                            height: 100,
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                2 *
                                                1.3,
                                            padding: EdgeInsets.symmetric(
                                              horizontal: 10,
                                            ),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Container(
                                                      width: 150,
                                                      child: Text(
                                                        _name(snapshot
                                                            .data[index]),
                                                        style: TextStyle(
                                                          fontSize: 17,
                                                          color: lightText,
                                                        ),
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        maxLines: 1,
                                                        softWrap: false,
                                                      ),
                                                    ),
                                                    Container(
                                                      child: InkWell(
                                                        onTap: () async {
                                                          setState(() {
                                                            deleteMenuItem(
                                                                snapshot.data[
                                                                        index]
                                                                    ['id']);
                                                          });
                                                        },
                                                        child: Icon(
                                                            Feather.trash,
                                                            color: Colors.red,
                                                            size: 15),
                                                      ),
                                                    ),
                                                  ],
                                                ),

                                                // _section(snapshot.data[index]) !=
                                                //         ''
                                                //     ? MenuSectionBadge(
                                                //         badgeText: _section(
                                                //             snapshot.data[index]))
                                                //     : Container(),
                                                // SizedBox(height: 5),
                                                SizedBox(
                                                  height: 5,
                                                ),
                                                Text(
                                                  '\u{20B9} ${_price(snapshot.data[index])}',
                                                  style: TextStyle(
                                                      fontSize: 25,
                                                      color: lightText),
                                                ),
                                                SizedBox(
                                                  height: 5,
                                                ),
                                                Row(
                                                  children: [
                                                    Container(
                                                      padding: EdgeInsets.only(
                                                        right: 15,
                                                      ),
                                                      child: Text(
                                                        "in Stock ?",
                                                        style: TextStyle(
                                                          color: Colors.white,
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      child: FlutterSwitch(
                                                        width: 100.0,
                                                        height: 40.0,
                                                        valueFontSize: 15.0,
                                                        toggleSize: 30.0,
                                                        value:
                                                            snapshot.data[index]
                                                                ['in_stock'],
                                                        borderRadius: 20.0,
                                                        padding: 8.0,
                                                        showOnOff: true,
                                                        activeText: 'Yes',
                                                        activeColor:
                                                            successColor,
                                                        inactiveText: 'No',
                                                        inactiveColor:
                                                            primaryColor,
                                                        onToggle: (val) {
                                                          setState(() {
                                                            inStock = val;
                                                          });

                                                          updateMenu(
                                                            val,
                                                            snapshot.data[index]
                                                                ['id'],
                                                            snapshot.data[index]
                                                                ['name'],
                                                            snapshot.data[index]
                                                                ['price'],
                                                            snapshot.data[index]
                                                                ['section'],
                                                            '',
                                                            '',
                                                          );
                                                        },
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            )),
                                      ],
                                    ),
                                  );
                                });
                          }
                          return Container();
                        }),
                  ),
                ]),
          );
        });
  }

  updateMenu(bool inStock, itemId, itemName, itemPrice, itemCategory,
      imageBase64, imageMime) async {
    final response =
        await http.patch(Uri.parse(baseURL + '/business/menu-item'),
            headers: <String, String>{
              'Content-Type': 'application/json; charset=UTF-8',
            },
            body: jsonEncode(<String, dynamic>{
              'image_mime': imageMime,
              'image_base64': imageBase64,
              'name': itemName,
              'price': itemPrice,
              'item_id': itemId,
              'in_stock': inStock,
            }));
    if (response.statusCode == 200) {
      final res = jsonDecode(response.body);
      print(res);

      return true;

      // return Album.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to update menu item');
    }
  }
}
