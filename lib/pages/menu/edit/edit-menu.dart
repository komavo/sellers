import 'dart:convert';
import 'dart:io';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:komavo_seller/pages/menu/api.dart';
import 'package:komavo_seller/pages/menu/menu_page.dart';

import 'package:komavo_seller/utils/buttons.dart';
import 'package:komavo_seller/utils/variables.dart';
import 'package:mime/mime.dart';
import 'package:select_form_field/select_form_field.dart';

import '../add_menu_item.dart';

class EditMenu extends StatefulWidget {
  final inStock;
  final itemId;
  final itemName;

  final itemPrice;
  final itemCategory;

  const EditMenu({
    required this.inStock,
    required this.itemId,
    required this.itemName,
    required this.itemPrice,
    required this.itemCategory,
  });

  @override
  _EditMenuState createState() => _EditMenuState();
}

class _EditMenuState extends State<EditMenu> {
  bool isComplete = false;

  void showInSnackBar(String value) {
    // ignore: deprecated_member_use
    Scaffold.of(context).showSnackBar(
      new SnackBar(
        content: new Text(value),
      ),
    );
  }

  void _pushAddMenuItem() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => AddMenuItem()));
  }

  updateMenu(bool inStock, itemId, itemName, itemPrice, itemCategory,
      imageBase64, imageMime) async {
    var body = jsonEncode(<String, dynamic>{
      'in_stock': inStock,
      'item_id': itemId,
      'name': itemName,
      'price': itemPrice,
      'section': itemCategory,
      'image_mime': imageMime,
      'image_base64': imageBase64,
    });
    final response =
        await http.patch(Uri.parse(baseURL + '/business/menu-item'),
            headers: <String, String>{
              'Content-Type': 'application/json; charset=UTF-8',
            },
            body: body);
    print(body);
    if (response.statusCode == 200) {
      final res = jsonDecode(response.body);
      print(res);
      fetchMenuItem();

      showInSnackBar("Successfully edited :   ${widget.itemName}");

      Navigator.of(context).push(MaterialPageRoute(
          builder: (ctx) => MenuPage(
                onAddMenuItem: _pushAddMenuItem,
              )));

      // return Album.fromJson(jsonDecode(response.body));
    } else {
      showInSnackBar("Something Went Wrong");
      throw Exception('Failed to update menu item');
    }
  }

  late TextEditingController nameController;
  late TextEditingController priceController;

  bool isLoading = false;
  String imagePath = "";

  final picker = ImagePicker();

  fetchCategories() async {
    await http.get(Uri.parse(baseURL + '/business/menu-section'));

    return true;
  }

  @override
  void initState() {
    fetchMenuItem();

    nameController = TextEditingController(text: widget.itemName);
    priceController = TextEditingController(text: widget.itemPrice.toString());
    super.initState();
  }

  sendEditMenuItem() async {
    setState(() {
      isLoading = true;
    });
    var imageMime = null;
    var image64 = null;
    if (imagePath != '') {
      image64 = base64Encode(File(imagePath).readAsBytesSync());
      imageMime = lookupMimeType(imagePath);
    }
    await updateMenu(
      widget.inStock,
      widget.itemId,
      nameController.text,
      priceController.text,
      widget.itemCategory,
      '',
      '',
    );

    print(widget.inStock.toString());
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: 1,
        itemBuilder: (context, snapshot) {
          return Container(
            color: Color(0xff111111),
            padding: EdgeInsets.symmetric(
              vertical: 20,
              horizontal: 20,
            ),
            height: 500.h,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 15,
                ),
                Container(
                  child: Text(
                    'Edit Menu Items',
                    style: TextStyle(
                      color: primaryColor,
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  child: Text(
                    'Name :  ${widget.itemName.toString()}',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Color(0xff222222),
                    ),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Center(
                        child: TextFormField(
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'This is required';
                        }
                        return null;
                      },
                      textAlign: TextAlign.left,
                      controller: nameController,
                      style: TextStyle(fontSize: 20, color: lightText),
                      decoration: InputDecoration(
                        labelStyle: TextStyle(color: primaryColor),
                        hintText: 'New Name',
                        hintStyle:
                            TextStyle(fontSize: 20, color: Color(0xff666666)),
                        contentPadding: EdgeInsets.all(10),
                        border: InputBorder.none,
                      ),
                      onSaved: (String? value) {},
                    ))),
                SizedBox(
                  height: 20,
                ),
                Container(
                  child: isLoading
                      ? Text(
                          'Price :  \u{20B9} ${widget.itemPrice.toString()}',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        )
                      : Text(
                          'Price :  \u{20B9} ${widget.itemPrice.toString()}',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Color(0xff222222),
                    ),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Center(
                        child: TextFormField(
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'This is required';
                        }
                        return null;
                      },
                      textAlign: TextAlign.left,
                      controller: priceController,
                      style: TextStyle(fontSize: 20, color: lightText),
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        new LengthLimitingTextInputFormatter(10),
                        FilteringTextInputFormatter.allow(RegExp(r'[0-9]|10'))
                      ],
                      decoration: InputDecoration(
                        labelStyle: TextStyle(color: primaryColor),
                        hintText: 'New Price',
                        hintStyle:
                            TextStyle(fontSize: 20, color: Color(0xff666666)),
                        contentPadding: EdgeInsets.all(10),
                        border: InputBorder.none,
                      ),
                      onSaved: (String? value) {},
                    ))),
                SizedBox(
                  height: 30,
                ),
                isLoading
                    ? Container(
                        alignment: Alignment.centerRight,
                        child: CustomButton(
                            buttonText: 'Please wait...',
                            type: 'primary',
                            callback: () {},
                            icon: Icon(Feather.loader)),
                      )
                    : Container(
                        alignment: Alignment.centerRight,
                        child: CustomButton(
                          buttonText: 'Add item',
                          type: 'primary',
                          callback: () async {
                            setState(() {
                              isLoading = true;
                            });

                            await sendEditMenuItem();

                            print(widget.inStock);
                            setState(() {
                              isLoading = false;
                            });
                          },
                          icon: Icon(Feather.check),
                        ),
                      )
              ],
            ),
          );
        });
  }
}
