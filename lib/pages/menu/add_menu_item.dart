import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:komavo_seller/pages/menu/api.dart';
import 'package:komavo_seller/pages/menu/menu_page.dart';
import 'package:select_form_field/select_form_field.dart';
import 'package:mime/mime.dart';
import 'package:http/http.dart' as http;
import 'dart:io';
import 'dart:convert';

import '../../utils/variables.dart';
import '../../utils/buttons.dart';
import '../../utils/providers.dart';
import '../../utils/shared_preferences.dart';
import '../../screens/main_screen.dart';

class AddMenuItem extends StatefulWidget {
  const AddMenuItem({Key? key}) : super(key: key);

  @override
  _AddMenuItemState createState() => _AddMenuItemState();
}

class _AddMenuItemState extends State<AddMenuItem> {
  final nameController = TextEditingController();
  final priceController = TextEditingController();
  String? _selectedCategory;
  bool isLoading = false;

  List<Map<String, dynamic>>? _categories;

  String imagePath = "";
  bool noImageError = false;
  final picker = ImagePicker();

  final _formKey = GlobalKey<FormState>();

  fetchCategories() async {
    var response =
        await http.get(Uri.parse(baseURL + '/business/menu-section'));
    setState(() {
      _categories = (jsonDecode(response.body)["data"] as List)
          .map((e) => e as Map<String, dynamic>)
          .toList();
    });
    return true;
  }

  sendCreateMenuItem() async {
    setState(() {
      isLoading = true;
    });
    var imageMime = null;
    var image64 = null;
    if (imagePath != '') {
      image64 = base64Encode(File(imagePath).readAsBytesSync());
      imageMime = lookupMimeType(imagePath);
    }
    var res = await createMenuItem(image64, imageMime, nameController.text,
        priceController.text, _selectedCategory);
    if (res) {
      // Provider.of<MenuProvider>(context, listen: false).refreshItems();
      Navigator.pop(context);
    }
  }

  @override
  void initState() {
    super.initState();
    fetchCategories();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: darkBGColor,
      body: SingleChildScrollView(
        child: Container(
            padding: EdgeInsets.all(30),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 50),
                    child: Row(
                      children: [
                        InkWell(
                          child: Icon(
                            Feather.arrow_left,
                            size: 25,
                            color: lightText,
                          ),
                          onTap: () => {Navigator.of(context).pop()},
                        ),
                        Spacer(),
                        Text(
                          'Add Menu Item',
                          style: TextStyle(fontSize: 25, color: lightText),
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 50),
                  imagePath == ""
                      ? Image.asset(
                          'assets/images/menu-item-placeholder.png',
                          width: 250,
                        )
                      : Image.file(
                          File(imagePath),
                          width: 250,
                        ),
                  SizedBox(height: 20),
                  noImageError
                      ? Text(
                          "An image representing your restaurant is required",
                          style: TextStyle(color: Colors.redAccent),
                        )
                      : Container(),
                  Center(
                      child: CustomButton(
                    buttonText: 'Upload an image',
                    type: 'secondary',
                    icon: Icon(Feather.upload_cloud),
                    callback: () async {
                      final pickedFile =
                          await picker.pickImage(source: ImageSource.gallery);
                      if (pickedFile != null) {
                        File? croppedFile = await ImageCropper.cropImage(
                          sourcePath: pickedFile.path,
                          aspectRatioPresets: [
                            CropAspectRatioPreset.square,
                            // CropAspectRatioPreset.ratio3x2,
                            // CropAspectRatioPreset.original,
                            // CropAspectRatioPreset.ratio4x3,
                            // CropAspectRatioPreset.ratio16x9
                          ],
                          androidUiSettings: AndroidUiSettings(
                            toolbarTitle: 'Crop Image',
                            toolbarColor: primaryColor,
                            toolbarWidgetColor: Colors.white,
                            activeControlsWidgetColor: primaryColor,
                            initAspectRatio: CropAspectRatioPreset.original,
                            lockAspectRatio: false,
                          ),
                          iosUiSettings: IOSUiSettings(
                            minimumAspectRatio: 1.0,
                          ),
                        );
                        if (croppedFile != null) {
                          setState(() {
                            imagePath = croppedFile.path;
                          });
                        }
                      }
                    },
                  )),
                  Container(
                      margin: EdgeInsets.only(top: 20),
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Center(
                          child: SelectFormField(
                        type: SelectFormFieldType.dropdown, // or can be dialog
                        items: _categories,
                        style: TextStyle(color: lightText),
                        decoration: InputDecoration(
                            hoverColor: Colors.black,
                            focusColor: Colors.black,
                            labelText: 'Select Category',
                            labelStyle: TextStyle(color: primaryColor)),
                        onChanged: (val) => {
                          setState(() {
                            _selectedCategory = val;
                          }),
                        },
                      ))),
                  Container(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      margin: EdgeInsets.only(top: 20),
                      child: Center(
                          child: TextFormField(
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'This is required';
                          }
                          return null;
                        },
                        textAlign: TextAlign.left,
                        controller: nameController,
                        style: TextStyle(fontSize: 20, color: lightText),
                        decoration: InputDecoration(
                          labelText: 'Item name',
                          labelStyle: TextStyle(color: primaryColor),
                          hintText: 'Name of the item',
                          hintStyle:
                              TextStyle(fontSize: 20, color: Color(0xff666666)),
                          contentPadding: EdgeInsets.all(10),
                          border: InputBorder.none,
                        ),
                        onSaved: (String? value) {},
                      ))),
                  Container(
                      margin: EdgeInsets.only(top: 20),
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: Center(
                          child: TextFormField(
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'This is required';
                          }
                          return null;
                        },
                        textAlign: TextAlign.left,
                        controller: priceController,
                        style: TextStyle(fontSize: 20, color: lightText),
                        keyboardType: TextInputType.number,
                        inputFormatters: <TextInputFormatter>[
                          new LengthLimitingTextInputFormatter(10),
                          FilteringTextInputFormatter.allow(RegExp(r'[0-9]|10'))
                        ],
                        decoration: InputDecoration(
                          labelText: 'Price',
                          labelStyle: TextStyle(color: primaryColor),
                          hintText: 'Price in Rupees',
                          hintStyle:
                              TextStyle(fontSize: 20, color: Color(0xff666666)),
                          contentPadding: EdgeInsets.all(10),
                          border: InputBorder.none,
                        ),
                        onSaved: (String? value) {},
                      ))),
                  SizedBox(
                    height: 50,
                  ),
                  isLoading
                      ? CustomButton(
                          buttonText: 'Please wait...',
                          type: 'primary',
                          callback: () {},
                          icon: Icon(Feather.loader))
                      : CustomButton(
                          buttonText: 'Add item',
                          type: 'primary',
                          callback: () {
                            if (_formKey.currentState!.validate()) {
                              sendCreateMenuItem();
                            }
                          },
                          icon: Icon(Feather.check),
                        )
                ],
              ),
            )),
      ),
    );
  }

  
}
