import '../../utils/variables.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import '../../utils/shared_preferences.dart';
import '../../utils/api.dart';
import './menu_model.dart';

createMenuItem(
    imageBase64, imageMIME, itemName, itemPrice, itemCategory) async {
  var userId = await MySharedPreferences.instance.getStringValue('id');
  final response = await http.post(Uri.parse(baseURL + '/business/menu-item'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        'business_id': userId,
        'name': itemName,
        'price': itemPrice,
        'section': itemCategory,
        'image_base64': imageBase64,
        'image_mime': imageMIME,
      }));
  if (response.statusCode == 200) {
    final res = jsonDecode(response.body);
    return true;
    // return Album.fromJson(jsonDecode(response.body));
  } else {
    throw Exception('Failed to create menu item');
  }
}

deleteMenuItem(itemId) async {
  ApiController apiController = ApiController();
  var response = apiController.delete('/business/menu-item/$itemId');
  return true;
}

Future<List<dynamic>> fetchMenuItem() async {
  var userId = await MySharedPreferences.instance.getStringValue('id');
  var response = await http.get(Uri.parse(baseURL + '/business/menu/$userId'));

  // if (response.statusCode == 200) {
  //   final res = jsonDecode(response.body);
  //   print(res);
  //   await MySharedPreferences.instance
  //       .setBooleanValue('in_stock', res['data']['in_stock']);

  //   // return Album.fromJson(jsonDecode(response.body));
  // } else {
  //   throw Exception('Failed to load self details');
  // }
  return json.decode(response.body)['data'];
}

class MenuApi {
  static Future<List<Data>> getMenu(String query) async {
    var userId = await MySharedPreferences.instance.getStringValue('id');
    final url = Uri.parse(baseURL + '/business/menu/$userId');
    final response = await http.get(url);
    if (response.statusCode == 200) {
      final List menu = json.decode(response.body)['data'];

      return menu.map((json) => Data.fromJson(json)).where((data) {
        final catLower = data.section.toLowerCase();
        final titleLower = data.name.toLowerCase();
        final searchLower = query.toLowerCase();
        return titleLower.contains(searchLower) ||
            catLower.contains(searchLower);
      }).toList();
    } else {
      throw Exception();
    }
  }
}
