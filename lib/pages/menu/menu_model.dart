//@dart = 2.9

class Menu {
  int statusCode;
  bool error;
  bool success;
  String message;
  List<Data> data;

  Menu({this.statusCode, this.error, this.success, this.message, this.data});

  Menu.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    error = json['error'];
    success = json['success'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status_code'] = this.statusCode;
    data['error'] = this.error;
    data['success'] = this.success;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int price;
  int businessId;
  String name;
  String section;
  String image;
  int id;
  bool inStock;
  int discount;

  Data({
    this.price,
    this.businessId,
    this.name,
    this.section,
    this.image,
    this.id,
    this.inStock,
    this.discount,
  });

  Data.fromJson(Map<String, dynamic> json) {
    price = json['price'];
    businessId = json['business_id'];
    name = json['name'];
    section = json['section'];
    image = json['image'];
    id = json['id'];
    inStock = json['in_stock'];
    discount = json['discount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['price'] = this.price;
    data['business_id'] = this.businessId;
    data['name'] = this.name;
    data['section'] = this.section;
    data['image'] = this.image;
    data['id'] = this.id;
    data['in_stock'] = this.inStock;
    data['discount'] = this.discount;
    return data;
  }
}
