import 'package:flutter/material.dart';
import 'package:komavo_seller/landing_screen.dart';
import 'package:komavo_seller/utils/shared_preferences.dart';

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Text(
              'Coming Soon',
              style: TextStyle(
                color: Colors.white60,
                fontSize: 30,
                fontFamily: 'Raleway',
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            'Our developers are working on it',
            style: TextStyle(
              color: Colors.white60,
              fontSize: 15,
              fontFamily: 'Raleway',
            ),
          ),
          SizedBox(
            height: 200,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                IconButton(
                    onPressed: () async {
                      await MySharedPreferences.instance.removeAll();
                      Navigator.of(context).pushReplacement(
                        MaterialPageRoute(
                            builder: (context) => LandingScreen()),
                      );
                    },
                    icon: Icon(
                      Icons.logout_rounded,
                      color: Colors.white,
                    )),
                Text(
                  'Logout',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                  ),
                )
              ],
            ),
          ),
          Text(
            'App version : Beta \(1.0.0\)',
            style: TextStyle(
              color: Colors.grey[500],
              fontSize: 15,
            ),
          )
        ],
      ),
    );
  }
}
