import '../../utils/variables.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

import '../../utils/shared_preferences.dart';


Future<List<dynamic>> fetchOrders() async {
  var userId = await MySharedPreferences.instance.getStringValue('id');
  var response = await http.get(Uri.parse(baseURL + '/business/order/list/$userId'));
  print(json.decode(response.body));
  return json.decode(response.body)['data'];
}
