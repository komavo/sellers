// @dart=2.9
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:komavo_seller/pages/menu/api.dart';
import 'package:komavo_seller/pages/orders/api.dart';
import 'package:http/http.dart' as http;
import 'package:komavo_seller/utils/providers.dart';
import 'package:provider/provider.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../utils/variables.dart';
import '../../utils/buttons.dart';
import '../../utils/api.dart';
import 'utils.dart';
import '../../controller/orders.dart';

class OrderListPage extends StatefulWidget {
  const OrderListPage({Key key}) : super(key: key);

  @override
  _OrderListPageState createState() => _OrderListPageState();
}

class _OrderListPageState extends State<OrderListPage> {
  var orderController = Get.put(OrderController());

  getButtonText(status) {
    switch (status) {
      case 'pending':
        return 'Confirm Order';
      case 'preparing':
        return 'Order Prepared';
      default:
        return 'Confirm Order';
    }
  }

  @override
  void initState() {
    fetchOrders();
    orderController.callOrdersMethod();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30)),
          color: darkBGColor,
          boxShadow: [
            new BoxShadow(
              color: Colors.black,
              offset: Offset(0, 2),
              blurRadius: 15.0,
            )
          ],
        ),
        width: double.infinity,
        margin: EdgeInsets.only(bottom: 20),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          SizedBox(height: 20),
          Container(
            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 30),
            child: Row(
              children: [
                Text('Orders',
                    style: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.bold,
                        color: lightText)),
                Spacer(),
              ],
            ),
          ),
          Expanded(child: Obx(() {
            return orderController.fetchLoading.value
                ? Center(
                    child: CircularProgressIndicator(
                    color: primaryColor,
                  ))
                : orderController.orders.length == 0
                    ? emptyOrderWidget()
                    : RefreshIndicator(
                        onRefresh: fetchOrders,
                        child: ListView.builder(
                            physics: BouncingScrollPhysics(),
                            scrollDirection: Axis.vertical,
                            itemCount: orderController.orders.length,
                            itemBuilder: (context, index) {
                              var data = orderController.orders;
                              final order = data[index];
                              if (data[index].status == 'pending' ||
                                  data[index].status == 'preparing') {
                                return Container(
                                    child: Container(
                                  margin: EdgeInsets.symmetric(
                                      vertical: 10, horizontal: 20),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: primaryColor,
                                  ),
                                  padding: EdgeInsets.all(5),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Text('#${order.orderId.toString()}',
                                              style: TextStyle(
                                                  color: lightText,
                                                  fontSize: 20)),
                                          Spacer(),
                                          statusBadge(order.status, false),
                                        ],
                                      ),
                                      Container(
                                          padding: EdgeInsets.only(
                                              top: 10, left: 10),
                                          child: Text('Items',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight:
                                                      FontWeight.bold))),
                                      orderItems(order.items),
                                      CustomButton(
                                          buttonText: getButtonText(
                                              order.status.toString()),
                                          type: order.status.toString() ==
                                                  'pending'
                                              ? 'secondary'
                                              : 'success',
                                          callback: () {
                                            // updateOrderStatus(index, order.orderId,
                                            //     order.status.toString());
                                            orderController.changeStatus(
                                                index,
                                                order.orderId,
                                                order.status.toString());
                                            // setState(() {});
                                            orderController.getallorders();
                                          },
                                          icon: Icon(Feather.check)),
                                    ],
                                  ),
                                ));
                              } else {
                                return Container(
                                  margin: EdgeInsets.symmetric(
                                      vertical: 10, horizontal: 20),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: darkColor,
                                  ),
                                  padding: EdgeInsets.all(5),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Text('#${order.orderId.toString()}',
                                              style: TextStyle(
                                                  color: lightText,
                                                  fontSize: 20)),
                                          Spacer(),
                                          statusBadge(order.status, true),
                                        ],
                                      ),
                                      Container(
                                          padding: EdgeInsets.only(
                                              top: 10, left: 10),
                                          child: Text('Items',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight:
                                                      FontWeight.bold))),
                                      orderItems(order.items),
                                    ],
                                  ),
                                );
                              }
                            }),
                      );
          })),
        ]));
  }

  Widget orderItems(items) {
    return Container(
        child: ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            padding: const EdgeInsets.all(8),
            itemCount: items.length,
            shrinkWrap: true,
            itemBuilder: (context, index) {
              final item = items[index];
              return Row(
                children: [
                  Text(item.name, style: TextStyle(color: Colors.white)),
                  Spacer(),
                  Text(' x ', style: TextStyle(color: lightText)),
                  Text(' ${item.quantity}',
                      style: TextStyle(color: Colors.white)),
                ],
              );
            }));
  }

  Widget statusBadge(status, isCompleted) {
    return Container(
        padding: EdgeInsets.symmetric(vertical: 3, horizontal: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: isCompleted ? successColor : Colors.white,
        ),
        child: Text(status.toUpperCase(),
            style: TextStyle(
                fontSize: 10,
                fontWeight: FontWeight.bold,
                color: isCompleted ? lightText : darkText)));
  }

  Widget emptyOrderWidget() {
    return Container(
      child: Center(
        child: Column(
          children: [
            SizedBox(height: 150),
            Image.asset('assets/images/no-order.png', width: 200),
            Text('No Orders', style: TextStyle(color: lightText)),
          ],
        ),
      ),
    );
  }
}
