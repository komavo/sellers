import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:provider/provider.dart';

import '../../utils/api.dart';
import '../../utils/buttons.dart';
import '../../utils/variables.dart';
import '../../utils/providers.dart';
import '../../screens/main_screen.dart';

import 'orders.dart';

class OrderItems extends StatelessWidget {
  final List<Map<String, dynamic>> items;
  const OrderItems({Key? key, required this.items}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        padding: const EdgeInsets.all(8),
        itemCount: items.length,
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            height: 50,
            child: Row(
              children: [
                Center(
                    child: Text(
                        '${items[index]['name']} (${items[index]['price']})',
                        style: TextStyle(color: Colors.white, fontSize: 18))),
                Spacer(),
                Center(
                  child: Text('x   ${items[index]['quantity'].toString()}',
                      style: TextStyle(color: Colors.white, fontSize: 18)),
                ),
                SizedBox(width: 10),
              ],
            ),
          );
        });
  }
}

class StatusBadge extends StatelessWidget {
  final String? status;
  final String? orderId;
  StatusBadge({Key? key, required this.orderId, required this.status})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        color: status != 'completed' ? Color(0xffFFC300) : successColor,
      ),
      padding: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
      child: Center(
        child: Text(StatusControl(orderId, status).getText(),
            style: TextStyle(
                color: (status == 'completed' ? Colors.white : Colors.black))),
      ),
    );
  }
}

class StatusControl {
  final String? status;
  final String? orderId;

  StatusControl(this.orderId, this.status);

  ApiController apiController = ApiController();

  OrderListPage orders = OrderListPage();

  getText() {
    switch (status) {
      case 'pending':
        {
          return 'Pending';
        }
      case 'preparing':
        {
          return 'Preparing';
        }
      case 'completed':
        {
          return 'Completed';
        }
      default:
        return 'Order';
    }
  }

  getButton(context, orderProvider) {
    switch (status) {
      case 'pending':
        {
          var body = <String, String>{
            'order_id': this.orderId!,
            'status': 'preparing'
          };
          return CustomButton(
              buttonText: 'Confirm Order',
              type: 'secondary',
              callback: () => {
                    apiController.post('/business/order/update', body),
                    orderProvider.refreshOrders(),                                    },
                    // orders.refresh();
              icon: Icon(Feather.check_circle, size: 18));
        }
      case 'preparing':
        {
          var body = <String, String>{
            'order_id': this.orderId!,
            'status': 'completed'
          };
          return CustomButton(
              buttonText: 'Order prepared',
              type: 'success',
              callback: () => {
                    apiController.post('/business/order/update', body),
                    orderProvider.refreshOrders(),
                  },
              icon: Icon(Feather.check_circle, size: 18));
        }
      case 'completed':
        {
          return Container();
        }
      default:
        Container();
    }
  }
}
