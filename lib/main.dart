//@dart =2.9

import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:new_version/new_version.dart';

import 'package:splashscreen/splashscreen.dart';
import 'utils/variables.dart';
import 'utils/shared_preferences.dart';
import './landing_screen.dart';
import 'screens/apply/holdingScreen.dart';
import 'screens/main_screen.dart';
import 'controller/orders.dart';

const AndroidNotificationChannel channel = AndroidNotificationChannel(
    'high_importance_channel',
    'High Importance Channel',
    'This channel is used for impotant notification',
    importance: Importance.high,
    playSound: true);

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

Future<void> _messageHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  print('background message ${message.notification.body}');
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_messageHandler);

  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  FirebaseMessaging messaging;
  bool userLoggedIn = false;
  bool isVerified = true;
  String token = '';

  var orderController = Get.put(OrderController());

  @override
  void initState() {
    super.initState();
    messaging = FirebaseMessaging.instance;
    messaging.getToken().then((value) {
      print('DEVICE TOKEN : $value');
      setState(() {
        token = value;
      });
    });
    checkVerificationStatus();
    updateDeviceToken();
    _checkVersion();
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification notification = message.notification;
      AndroidNotification android = message.notification?.android;
      if (notification != null && android != null) {
        orderController.callOrdersMethod();
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                channel.description,
                color: primaryColor,
                playSound: true,
                icon: '@mipmap/ic_launcher',
              ),
            ));
      }

      FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
        RemoteNotification notification = message.notification;
        AndroidNotification android = message.notification?.android;
        if (notification != null && android != null) {
          Get.to(MainScreen(selectedIndex: 2));
        }
      });
    });
  }

  void _checkVersion() async {
    final newVersion = NewVersion(
      androidId: "com.snapchat.android",
    );
    final status = await newVersion.getVersionStatus();
    newVersion.showUpdateDialog(
      context: context,
      versionStatus: status,
      dialogTitle: "UPDATE!!!",
      dismissButtonText: "Skip",
      dialogText: "Please update the app from " +
          "${status.localVersion}" +
          " to " +
          "${status.storeVersion}",
      dismissAction: () {
        SystemNavigator.pop();
      },
      updateButtonText: "Lets update",
    );

    print("DEVICE : " + status.localVersion);
    print("STORE : " + status.storeVersion);
  }

  checkVerificationStatus() async {
    var checkId = await MySharedPreferences.instance.containsKey('id');
    if (checkId != false) {
      var userId = await MySharedPreferences.instance.getStringValue('id');
      final response = await http.get(
          Uri.parse(baseURL + '/business/detail/$userId'),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          });
      if (response.statusCode == 200) {
        final temp = jsonDecode(response.body);
        print(temp);
        setState(() {
          isVerified = temp['data']['is_verified'];
        });
        return;
      } else {
        throw Exception('There was an error processing');
      }
    }
  }

  updateDeviceToken() async {
    var checkId = await MySharedPreferences.instance.containsKey('id');
    if (checkId != false) {
      var checkDeviceToken =
          await MySharedPreferences.instance.containsKey('device_token');
      var old_token = null;
      if (checkDeviceToken != false) {
        old_token =
            await MySharedPreferences.instance.getStringValue('device_token');
      }
      var userId = await MySharedPreferences.instance.getStringValue('id');
      var response = await http.post(Uri.parse(baseURL + '/auth/device-token'),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(<String, dynamic>{
            'token': token,
            'user_type': 'business',
            'user_id': userId,
            'old_token': old_token
          }));
      if (response.statusCode == 200) {
        final temp = jsonDecode(response.body);
        print(temp);
        await MySharedPreferences.instance
            .setStringValue('device_token', token);
        return true;
      } else {
        throw Exception('There was an error processing');
      }
    } else
      return false;
  }

  _MyAppState() {
    MySharedPreferences.instance
        .getBooleanValue("loggedIn")
        .then((value) => setState(() {
              print(value);
              userLoggedIn = value;
            }));
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: new ThemeData(fontFamily: 'Raleway'),
      home: ScreenUtilInit(
          designSize: Size(414, 896),
          builder: () => new SplashScreen(
              seconds: 2,
              navigateAfterSeconds: (userLoggedIn
                  // ? HomeScreen()
                  ? MainScreen(
                      selectedIndex: 0,
                    )
                  : isVerified
                      ? LandingScreen()
                      : HoldingScreen()),
              image: new Image.asset('assets/images/komavo_secondary.png'),
              backgroundColor: primaryColor,
              styleTextUnderTheLoader: new TextStyle(),
              photoSize: 150.0,
              loaderColor: Colors.white)),
    );
  }
}
