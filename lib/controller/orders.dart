// @dart=2.9
import 'dart:convert';
import 'package:get/get.dart';
import 'package:komavo_seller/pages/orders/utils.dart';
import 'package:komavo_seller/utils/api.dart';
import 'package:komavo_seller/utils/shared_preferences.dart';
import 'dart:math';
import '../models/orderModel.dart';

class OrderController extends GetxController {
  ApiController apiController = ApiController();

  // RxList<Order> _orders = <Order>[].obs;
  RxList<OrderModel> orders = <OrderModel>[].obs;
  var fetchLoading = false.obs;

  void onInit() {
    callOrdersMethod();
    super.onInit();
  }

  callOrdersMethod() async {
    try {
      fetchLoading.value = true;
      var result = await getallorders();
      if (result != null) {
        orders.assignAll(result);
      } else {
        print('no data');
      }
    } finally {
      fetchLoading.value = false;
    }
    update();
  }

  Future<List<OrderModel>> getallorders() async {
    var userId = await MySharedPreferences.instance.getStringValue('id');
    var response = await apiController.get('/business/order/list/$userId');
    return RxList<OrderModel>.from(
        response['data'].map((e) => new OrderModel.fromJson(e)).toList());
  }

  void changeStatus(index, orderId, status) async {
    var newStatus = '';
    switch (status) {
      case 'pending':
        {
          newStatus = 'preparing';
          break;
        }
      case 'preparing':
        {
          newStatus = 'completed';
          break;
        }
      default:
        newStatus = 'pending';
    }
    var body = <String, String>{
      'order_id': orderId.toString(),
      'status': newStatus
    };
    var response = await apiController.post('/business/order/update', body);
    callOrdersMethod();
  }
}
