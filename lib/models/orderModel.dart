// @dart=2.9
import 'dart:convert';

List<OrderModel> OrderModelFromJson(String str) =>
    List<OrderModel>.from(json.decode(str).map((x) => OrderModel.fromJson(x)));

String OrderModelToJson(List<OrderModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OrderModel {
  OrderModel({
    this.partial,
    this.id,
    this.deliveryCharge,
    this.businessId,
    this.status,
    this.consumerId,
    this.total,
    this.orderId,
    this.message,
    this.updatedAt,
    this.createdAt,
    this.items,
  });

  bool partial;
  int id;
  int deliveryCharge;
  int businessId;
  String status;
  int consumerId;
  int total;
  String orderId;
  String message;
  DateTime updatedAt;
  DateTime createdAt;
  List<Item> items;

  factory OrderModel.fromJson(Map<String, dynamic> json) => OrderModel(
        partial: json["_partial"],
        id: json["id"],
        deliveryCharge: json["delivery_charge"],
        businessId: json["business_id"],
        status: json["status"],
        consumerId: json["consumer_id"],
        total: json["total"],
        orderId: json["order_id"],
        message: json["message"],
        updatedAt: DateTime.parse(json["updated_at"]),
        createdAt: DateTime.parse(json["created_at"]),
        items: List<Item>.from(json["items"].map((x) => Item.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "_partial": partial,
        "id": id,
        "delivery_charge": deliveryCharge,
        "business_id": businessId,
        "status": status,
        "consumer_id": consumerId,
        "total": total,
        "order_id": orderId,
        "message": message,
        "updated_at": updatedAt.toIso8601String(),
        "created_at": createdAt.toIso8601String(),
        "items": List<dynamic>.from(items.map((x) => x.toJson())),
      };
}

class Item {
  Item({
    this.partial,
    this.id,
    this.businessId,
    this.name,
    this.price,
    this.section,
    this.image,
    this.quantity,
  });

  bool partial;
  int id;
  int businessId;
  String name;
  int price;
  String section;
  String image;
  int quantity;

  factory Item.fromJson(Map<String, dynamic> json) => Item(
        partial: json["_partial"],
        id: json["id"],
        businessId: json["business_id"],
        name: json["name"],
        price: json["price"],
        section: json["section"],
        image: json["image"],
        quantity: json["quantity"],
      );

  Map<String, dynamic> toJson() => {
        "_partial": partial,
        "id": id,
        "business_id": businessId,
        "name": name,
        "price": price,
        "section": section,
        "image": image,
        "quantity": quantity,
      };
}
